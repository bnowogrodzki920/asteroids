using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class Ship : WorldObject, IPlayerInputListener
{
    [SerializeField] private float rotationSpeed = 1;
    [SerializeField] private float moveSpeed = 1;
    [SerializeField] private float bulletsPerSecond = 1;

    private float shootingTimer = 0;

    [Inject]
    public void Inject(Settings settings)
    {
        rotationSpeed = settings.rotationSpeed;
        moveSpeed = settings.moveSpeed;
        bulletsPerSecond = settings.bulletsPerSecond;
    }


    public void OnMove(float value)
    {
        value *= moveSpeed;
        value *= Time.deltaTime;
        transform.position += transform.up * value;
    }

    public void OnRotate(float angle)
    {
        angle *= rotationSpeed;
        angle *= Time.deltaTime;
        transform.Rotate(Vector3.forward, angle);
    }

    public void OnShoot()
    {
        float bulletsCount = 60f / bulletsPerSecond;
        bulletsCount = bulletsCount / 60;

        shootingTimer -= Time.deltaTime;

        if(shootingTimer < 0)
        {
            shootingTimer += bulletsCount;

            //Shoot here
        }
    }

    [System.Serializable]
    public class Settings
    {
        public float rotationSpeed = 150;
        public float moveSpeed = 5;
        public float bulletsPerSecond = 30;
    }
}
