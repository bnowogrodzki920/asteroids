﻿using Zenject;
using UnityEngine;


[CreateAssetMenu(fileName = "GameSettings", menuName = "Installers/GameSettings")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    public Ship.Settings shipSettings;

    public override void InstallBindings()
    {
        Container.BindInstance(shipSettings).AsSingle();
    }
}