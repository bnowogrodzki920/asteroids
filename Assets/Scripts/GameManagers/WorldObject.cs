﻿using UnityEngine;

public abstract class WorldObject : MonoBehaviour
{
    [SerializeField] protected Vector2 border;

    private void Update()
    {
        if(transform.position.x >= border.x)
        {
            transform.position = new Vector3(-border.x , transform.position.y);
        }
        else if(transform.position.x <= -border.x)
        {
            transform.position = new Vector3(border.x , transform.position.y);
        }
        if(transform.position.y >= border.y)
        {
            transform.position = new Vector3(transform.position.x, -border.y);
        }
        else if(transform.position.y <= -border.y)
        {
            transform.position = new Vector3(transform.position.x, border.y);
        }
    }
}
