﻿using EventsBus;
using System.Collections;
using UnityEngine;
using Zenject;

public class EventBusInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<IEventBusFacade>().FromSubContainerResolve().ByMethod(InstallSystem).AsSingle();
    }

    private void InstallSystem(DiContainer subcontainer)
    {
        subcontainer.Bind<IEventBusFacade>().AsSingle();
        subcontainer.Bind<IEventBus>().AsSingle();
    }
}