﻿using EventsBus;

namespace EventsBus
{
    public interface IEventBus
    {
        public void RegisterListener<TEvent>(IListener listener) where TEvent : IEvent;
        public void UnregisterListener<TEvent>(IListener listener) where TEvent : IEvent;
        public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : IEvent;
    }
}