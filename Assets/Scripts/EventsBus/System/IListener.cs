﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsBus
{
    public interface IListener
    {
        void OnEvent(IEvent eventData);
    }
}
