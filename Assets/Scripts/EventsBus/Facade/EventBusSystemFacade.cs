using EventsBus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EventBusSystemFacade : IEventBusFacade
{
    IEventBus eventBus;

    public EventBusSystemFacade(IEventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : IEvent
    {
        eventBus.InvokeEvent(eventData);
    }

    public void RegisterListener<TEvent>(IListener listener) where TEvent : IEvent
    {
        eventBus.RegisterListener<TEvent>(listener);
    }

    public void UnregisterListener<TEvent>(IListener listener) where TEvent : IEvent
    {
        eventBus.UnregisterListener<TEvent>(listener);
    }
}

public interface IEventBusFacade
{
    public void RegisterListener<TEvent>(IListener listener) where TEvent : IEvent;
    public void UnregisterListener<TEvent>(IListener listener) where TEvent : IEvent;
    public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : IEvent;
}
