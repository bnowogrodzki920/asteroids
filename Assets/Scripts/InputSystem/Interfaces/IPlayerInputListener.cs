﻿internal interface IPlayerInputListener
{
    public void OnMove(float value);
    public void OnRotate(float angle);
    public void OnShoot();
}