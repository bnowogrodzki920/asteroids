﻿using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class InputSystem : MonoBehaviour, IInputSystem
{
    [SerializeField] private PlayerInput playerInput;

    private PlayerInputActionsSettings inputActions;
    private IPlayerInputListener playerInputListener;

    float moveValue = 0f;
    float rotationAngle = 0f;

    [Inject]
    private void Inject(IPlayerInputListener playerInputListener)
    {
        this.playerInputListener = playerInputListener;
    }

    private void Awake() 
    { 
        inputActions = new PlayerInputActionsSettings();

        inputActions.BasicControls.Enable();
        playerInput.ActivateInput();
    }

    private void Update()
    {
        if (inputActions.BasicControls.Movement.IsPressed())
        {
            moveValue = inputActions.BasicControls.Movement.ReadValue<float>();
            playerInputListener.OnMove(moveValue);
        }

        if (inputActions.BasicControls.Rotate.IsPressed())
        {
            rotationAngle = inputActions.BasicControls.Rotate.ReadValue<float>();
            playerInputListener.OnRotate(rotationAngle);
        }

        if (inputActions.BasicControls.Shoot.IsPressed())
        {
            playerInputListener.OnShoot();
        }
    }
}
